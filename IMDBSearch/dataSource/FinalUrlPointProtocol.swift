//
//  FinalUrlPointProtocol.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 15/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import Foundation

protocol FinalUrlPointProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var request: URLRequest { get }
}
