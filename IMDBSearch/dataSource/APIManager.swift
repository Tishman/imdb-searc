//
//  APIManager.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 15/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import Foundation


final class APIManager: APIManagerProtocol {
 
    var apiKey: String
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    //MARK: - Method to fetch data for search result list
    func fetchMovieSearchResult(movieTitle: String, completionHandler: @escaping ((APIResult<MovieSearch>) -> Void)) {
        let request = OMDbType.SearchResult(apiKey: self.apiKey, movieTitle: movieTitle).request

        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                guard let data = data else { return }
                let movieSearch = try JSONDecoder().decode(MovieSearch.self, from: data)
                completionHandler(APIResult.SuccessGetMovieSearch(movieSearch))
            } catch let jsonError as NSError {
                completionHandler(APIResult.Failure(jsonError))
            }
        }.resume()
    }
    
    //MARK: - Method to fetch data about movie
    func fetchMovieInfo(movieId: String, completionHandler: @escaping ((APIResult<Movie>) -> Void)) {
        let request = OMDbType.MovieInfo(apiKey: self.apiKey, movieId: movieId).request
        
       URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let movie = try JSONDecoder().decode(Movie.self, from: data!)
                    completionHandler(APIResult.SuccessGetMovie(movie))
                } catch let jsonError as NSError {
                    completionHandler(APIResult.Failure(jsonError))
                }
        }.resume()
    }

//MARK: - Construct Request
enum OMDbType: FinalUrlPointProtocol {
    var baseURL: URL {
        return URL(string: "http://www.omdbapi.com/")!
    }
    
    var path: String {
        switch self {
        case .SearchResult(let apiKey, let movieTitle):
            return "?s=\(movieTitle)&apikey=\(apiKey)"
        case .MovieInfo(let apiKey, let movieId):
            return "?i=\(movieId)&plot=full&apikey=\(apiKey)"
        default:
            break
        }
    }
    
    var request: URLRequest {
        let url = URL(string: path, relativeTo: baseURL)
        return URLRequest(url: url!)
    }
    
    case SearchResult(apiKey: String, movieTitle: String)
    case MovieInfo(apiKey: String, movieId: String)
    }

}
