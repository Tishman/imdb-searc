//
//  APIManager.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 15/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import Foundation

enum APIResult<T: Decodable> {
    case SuccessGetMovie(Movie)
    case SuccessGetMovieSearch(MovieSearch)
    case Failure(Error)
}

protocol APIManagerProtocol {
    
    var apiKey: String { get }
    
    func fetchMovieSearchResult(movieTitle: String, completionHandler: @escaping ((APIResult<MovieSearch>) -> Void))
    
    func fetchMovieInfo(movieId: String, completionHandler: @escaping ((APIResult<Movie>) -> Void))
}
