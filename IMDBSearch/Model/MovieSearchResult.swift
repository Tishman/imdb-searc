//
//  MovieSearchResult.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 15/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import Foundation

struct MovieSearch: Decodable {
    let Search: [MovieSearchResult]
}

struct MovieSearchResult: Decodable {    
    let imdbID: String
    let Title: String
}
