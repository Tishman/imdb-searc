//
//  Movie.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 15/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    let Title: String
    let imdbRating: String
    let Country: String
    let Director: String
    let Actors: String
    let Plot: String
    let Poster: String
}
