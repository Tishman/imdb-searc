//
//  SearchTableViewCell.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 17/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateCell (title: String) {
        self.titleLabel.text = title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
