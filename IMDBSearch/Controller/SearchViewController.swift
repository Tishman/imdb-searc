//
//  SearchViewController.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 16/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import UIKit
import Foundation

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
                            UISearchBarDelegate {
    
    @IBOutlet weak var noResultLabel: UILabel!
    @IBOutlet weak var searchText: UISearchBar!
    @IBOutlet weak var movieTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let apiKey = "cd4901de"
    private lazy var apiManager = APIManager(apiKey: apiKey)
    private var movieSearchResultItems = [MovieSearchResult]()
    private let network = NetworkManager.sharedInstance
    private static let slugSafeCharacters = CharacterSet(charactersIn: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NetworkManager.isUnreachable { _ in
            self.present(self.lostConnectionToInternet(), animated: true, completion: nil)
        }
        
        movieTableView.dataSource = self
        movieTableView.delegate = self
        searchText.delegate = self
    }
    
    private func lostConnectionToInternet() -> UIAlertController {
        let alertController = UIAlertController(title: "Connection lost", message: "You are offline, pleas connect to the internet", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        return alertController
    }
    
    //MARK: - Fetch data
    private func fetchSearchResultData(inputData: String) {
        apiManager.fetchMovieSearchResult(movieTitle: inputData) { (result) in
            
            switch result {
            case .SuccessGetMovieSearch(let movieSearch):
                self.movieSearchResultItems = movieSearch.Search
                DispatchQueue.main.async {
                    self.movieTableView.reloadData()
                    self.activityIndicator.stopAnimating()
                    self.noResultLabel.isHidden = true
                    self.movieTableView.isHidden = false
                }
            case .Failure(let error as NSError):
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.noResultLabel.isHidden = false
                }

                let alertController = UIAlertController(title: "Unable to get data ", message: "\(error.localizedDescription)", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
            default:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetails" {
            if let destinationVC = segue.destination as? MovieViewController {
                let movieId = sender as? String
                destinationVC.movieId = movieId
                destinationVC.apiKey = self.apiKey
            }
        }
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieSearchResultItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let resultCell = movieTableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as? SearchTableViewCell {
            resultCell.updateCell(title: movieSearchResultItems[indexPath.row].Title)
            return resultCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NetworkManager.isUnreachable { _ in
            self.present(self.lostConnectionToInternet(), animated: true, completion: nil)
        }
        
        let movieId = movieSearchResultItems[indexPath.row].imdbID
        self.performSegue(withIdentifier: "showMovieDetails", sender: movieId)
    }
    
    //MARK: - SearchBar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        noResultLabel.isHidden = true
        if searchText == "" {
            movieTableView.isHidden = true
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         NetworkManager.isUnreachable { _ in
            self.present(self.lostConnectionToInternet(), animated: true, completion: nil)
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
        
        let correctUrl = convertedToSlug(url: self.searchText.text!)
        
        guard correctUrl != nil else {
            self.searchText.resignFirstResponder()
            return
        }
        
        self.activityIndicator.startAnimating()
        fetchSearchResultData(inputData: correctUrl!)
        
        DispatchQueue.main.async {
            self.movieTableView.reloadData()
        }
        
        if searchBar == searchText {
            self.searchText.resignFirstResponder()
        }
    }
    
    //MARK: - Method to ceate correct url
    private func convertedToSlug(url: String) -> String? {
        if let latin = url.applyingTransform(StringTransform("Any-Latin; Latin-ASCII; Lower;"), reverse: false) {
            let urlComponents = latin.components(separatedBy: SearchViewController.slugSafeCharacters.inverted)
            let result = urlComponents.filter { $0 != "" }.joined(separator: "+")
            
            if result.count > 0 {
                return result
            }
        }
        
        return nil
    }
}
