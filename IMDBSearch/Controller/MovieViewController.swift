//
//  MovieViewController.swift
//  IMDBSearch
//
//  Created by Роман Тищенко on 16/10/2018.
//  Copyright © 2018 Роман Тищенко. All rights reserved.
//

import UIKit

class MovieViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var direcotrLabel: UILabel!
    @IBOutlet weak var actorsLabel: UILabel!
    @IBOutlet weak var discriptionTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var movieId: String?
    var apiKey: String?
    private var posterUrl: String? {
        didSet {
            if posterUrl != "N/A" {
            let imageUrl = URL(string: posterUrl!)
                DispatchQueue.global().async {
                    if let posterData = try? Data(contentsOf: imageUrl!) {
                        DispatchQueue.main.async {
                            self.poster.image = UIImage(data: posterData)
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
            } else {
                self.poster.image = UIImage(named: "No_image_available.svg")
            }
            titleLabel.isHidden = false
            poster.isHidden = false
            ratingLabel.isHidden = false
            countryLabel.isHidden = false
            direcotrLabel.isHidden = false
            actorsLabel.isHidden = false
            discriptionTextView.isHidden = false
            discriptionTextView.isEditable = false
        }
    }
    
    private lazy var apiManager = APIManager(apiKey: apiKey!)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        
        guard let movieId = movieId else { return }
        
        fetchMovieInfoData(inputData: movieId)
    }
    
    private func updateView(movie: Movie) {

    }
    
    private func fetchMovieInfoData(inputData: String) {
        apiManager.fetchMovieInfo(movieId: inputData) { (result) in
            switch result {
            case .SuccessGetMovie(let movie):
                DispatchQueue.main.async {
                    self.titleLabel.text = movie.Title
                    self.ratingLabel.text = "Rating: " + movie.imdbRating
                    self.countryLabel.text = "Country: " + movie.Country
                    self.direcotrLabel.text = "Director: " + movie.Director
                    self.actorsLabel.text = "Actors: " + movie.Actors
                    self.discriptionTextView.text = movie.Plot
                    self.posterUrl = movie.Poster
                }
            case .Failure(let error as NSError):
                let alertController = UIAlertController(title: "Unable to get data ", message: "\(error.localizedDescription)", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
            default:
                break
            }
        }
    }
}

